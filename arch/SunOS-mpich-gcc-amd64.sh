export ARCH=amd64
export CC=gcc
export CXX=g++
export FC=gfortran
export F77=gfortran
export MPICC=mpicc
export MPICXX=mpicxx
export MPIFC=mpif90
export MPIF77=mpif77
export CFLAGS="-march=native -m64 -Wl,-rpath,${KTHOPTROOT}/lib -L${KTHOPTROOT}/lib -R${KTHOPTROOT}/lib"
export CXXFLAGS="-march=native -m64 -Wl,-rpath,${KTHOPTROOT}/lib -L${KTHOPTROOT}/lib -R${KTHOPTROOT}/lib"
export FFLAGS="-march=native -m64 -Wl,-rpath,${KTHOPTROOT}/lib -L${KTHOPTROOT}/lib -R${KTHOPTROOT}/lib"
export FCFLAGS="-march=native -m64 -Wl,-rpath,${KTHOPTROOT}/lib -L${KTHOPTROOT}/lib -R${KTHOPTROOT}/lib"
export MPICH2LIB_FFLAGS=""
export MPICH2LIB_FCFLAGS=""
