#-------------------------------------------------------------------------------
# Copyright 2012-2013 Aurelien Larcher: alarcher 
# Modified by Murtazo Nazarov: murtazo
#
#-------------------------------------------------------------------------------

BUILD_DIR=build
ARCHIVE_DIR=archive
PKGLIST+=scientificpython
PKGLIST+=instant
PKGLIST+=ffc
PKGLIST+=fiat
PKGLIST+=ufc
PKGLIST+=ufl
PKGLIST+=mpich
PKGLIST+=parmetis
PKGLIST+=petsc
PKGLIST+=gts

.DEFAULT: world

all: build 

clean:
	@echo "*** Clean";\
	for i in $(PKGLIST);\
	do rm -f $$i/.extract $$i/.fetch $$i/.build $$i/.config;\
	       rm -rf $$i/$(BUILD_DIR);\
	done;

distclean: clean
	@echo "*** Distclean"
	@rm -rf .fetch .extract .config .build $(BUILD_DIR) $(ARCHIVE_DIR)

fetch:
	@if test ! -e ".fetch"; \
	then echo "*** Fetch"; \
	    rm -f .extract .config .build; \
	    if test "$(PORTGIT)X" = "X" ;\
		then echo "***** Fetching from archive";\
	            if test ! -d $(ARCHIVE_DIR); then mkdir $(ARCHIVE_DIR); \
	            else rm -rf "$(ARCHIVE_DIR)/$(PORTSRC)"; fi; \
	            wget $(PORTURL) -P $(ARCHIVE_DIR) && touch ".fetch"; \
	    else echo "***** Fetching from bitbucket";\
                git clone --depth 1 --branch $(PORTVERSION) $(PORTGIT) && touch ".fetch"; \
	        mkdir -p $(BUILD_DIR) ;\
		mv $(PORTDIR) $(BUILD_DIR);\
	    fi;\
	fi;
	
extract: fetch
	@if test ! -e ".extract"; \
	then echo "*** Extract"; \
	    if test "$(PORTGIT)X" = "X" ;\
		then mkdir -p $(BUILD_DIR) ;\
		rm -f .config .build; \
		if test "X$(PORTSRC)" != "X`basename $(PORTSRC) .tar.gz`"; \
		then echo "*** Un-tgz it !"; \
			tar xvzf $(ARCHIVE_DIR)/$(PORTSRC) -C $(BUILD_DIR) && touch ".extract"; \
		elif test "X$(PORTSRC)" != "X`basename $(PORTSRC) .tar.bz2`" && touch ".extract"; \
		then echo "*** Un-tbz2 it !"; \
			tar xvjf $(ARCHIVE_DIR)/$(PORTSRC) -C $(BUILD_DIR); \
		fi; \
	    fi; \
	fi;
	
config: extract
	@if test "${KTHOPTROOT}X" = "X" ; \
	then \
		echo "Environment variable KTHOPTROOT is not set" && exit 1; \
	else \
		if test -d ${KTHOPTROOT}; \
		then \
			if test `stat -c %U ${KTHOPTROOT}` != `whoami` ; \
				then echo "Invalid permissions on ${KTHOPTROOT}" && exit 1; \
			fi; \
		else echo "Target install directory ${KTHOPTROOT} does not exist." && exit 1; \
		fi; \
	fi; 
	@if test "X" = "X$(CONFIGURE)"; \
	then echo "Configure command empty"; \
	fi;
	@if test ! -e ".config"; \
	then echo "*** Configure"; \
		rm -f ".build"; \
		cd $(BUILD_DIR)/$(PORTDIR) && $(CONFIGURE) && cd - && touch ".config"; \
	fi;

build: config
	@if test ! -e ".build"; \
	then echo "*** Build"; \
		cd $(BUILD_DIR)/$(PORTDIR) && $(BUILD) && cd - && touch ".build"; \
	fi;

install: build
	@echo "*** Install"
	@cd $(BUILD_DIR)/$(PORTDIR) && $(INSTALL)

world:
	@for p in $(PKGLIST); \
	do echo "=== Installing $$p"; \
		cd $$p && make install && cd -; \
	done;

distclean-all:
	@for p in $(PKGLIST); \
	do echo "=== Cleaning $$p"; \
		cd $$p && make distclean && cd -; \
	done;

